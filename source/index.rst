=================
The Mailman Suite
=================

The Mailman home page is http://www.list.org, and there is a community driven
wiki at http://wiki.list.org.

Mailman 3.1 was released on May 25, 2017. The Mailman Suite consists of 5
individual projects Below are links to documentation for each of the projects.

Those packages are copyrighted by the `Free Software Foundation`_ and
distributed under the terms of the `GNU General Public License (GPL) version
3`_ or later.

* `Mailman Core`_ - the mailing list manager core (required)
* `Postorius`_ - the adminstrative web user interface
* `MailmanClient`_ - the official REST API Python bindings
* `HyperKitty`_ - the web archiver
* `HyperKitty Mailman plugin`_ - archiver plugin for Core


The Installation Guide
======================

..  toctree::
    :maxdepth: 2

    prodsetup.rst

The User Guide
==============


..  toctree::
    :maxdepth: 2

    userguide.rst

Documentation for Mailman 3 List Owners and Site Administrators is not yet
complete.

.. The Community Guide
   ===================





The Contributor Guide
=====================

..  toctree::
    :maxdepth: 2

    devsetup.rst


.. _Free Software Foundation: http://www.fsf.org/
.. _GNU General Public License (GPL) version 3: http://www.gnu.org/licenses/quick-guide-gplv3.html
.. _Mailman Core: http://mailman.readthedocs.org/
.. _Postorius: http://postorius.readthedocs.org/
.. _MailmanClient: http://mailmanclient.readthedocs.org/
.. _HyperKitty: http://hyperkitty.readthedocs.org/
.. _`HyperKitty Mailman plugin`: https://gitlab.com/mailman/mailman-hyperkitty
