==========================
Mailman Suite Installation
==========================

This is a step-by-step installation guide for Mailman Suite, also sometimes
referred as Mailman 3 Suite or just Mailman 3. There are more than one ways to
have Mailman 3 running on your machine.

Mailman 3 in Docker
-------------------

Abhilash Raj maintains container images for Mailman 3 which you can use directly
without having to go through all the steps to download dependencies and
configuring Mailman. You can find the `detailed documentation`_ on how to use
them. If you have any issues using the container images, please report them
directly on the `Github repo for docker-mailman`_.


Distribution Packages
---------------------

Distribution specific packages for Mailman 3 are not yet available. There are
efforts going on to package for Debian based systems. If you would like to help
package Mailman 3 for your favorite Linux distro, please get in touch at
mailman-developers@python.org.


Dependencies
------------

  * Python 2.7 and Python3.5+ both. Mailman Core (v3.1) runs exclusively on
    Python3.4+, but the support for Python 3.4 might be deprecated in future
    versions. The web frontend (in Django) runs exclusively on Python 2.7.

  * MTA: Mailman 3 in theory would support any MTA or mail server which can send
    emails to Mailman over LMTP. Officially, there are configurations for
    Postfix_, Exim4_, qmail_ and sendmail_. Mailman Core has a fairly elaborate
    documentation on `setting up your MTA`_. Look below at `setting up mailman
    core`_ to find out the location of configuration file ``mailman.cfg`` which
    is mentioned in the documentation above.

    The Web Front-end is based on a Python web framework called Django. For
    email verification and sending out error logs, Django also must be
    configured to send out emails. Please have a look at how to `setup
    email backend for django`_. A simple configuration would look something
    like this for a mail server listening on localhost::

      # To be added to Django's settings.py

      EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
      EMAIL_HOST = 'localhost'
      EMAIL_PORT = 25
      EMAIL_HOST_USER = <username>
      EMAIL_HOST_PASSWORD = <password>

  * A `sass compiler`_. Syntactically Awesome Stylesheets or Sass is an
    extension of CSS, which is used to style webpages, that adds more power to
    the old CSS syntax. It allows using variables, nested rules, inline imports
    etc. which CSS originally doesn't support. Hyperkitty uses this to generate
    CSS styles.

    You can either use the Ruby implementation or C/C++ implementation. Please
    look at the `installation guide for sass`_ to see how you can get one.

    For apt based systems::

      $ sudo apt install ruby-sass

    For yum based systems try::

      $ sudo yum install rubygem-sass

    or::

      $ sudo dnf install rubygem-sass


    After installing this, you'd have to configure Django to use these
    compilers. A basic configuration would look like::

      # To be added to Django's settings.py

      COMPRESS_PRECOMPILERS = (
        ('text/x-scss', 'sass -t compressed {infile} {outfile}'),
        ('text/x-sass', 'sass -t compressed {infile} {outfile}'),
      )

    You can replace ``sass`` above with whatever is the name of the binary
    is. For the Ruby implementation it is generally ``sass``, for C/C++ it is
    ``sassc``.

  * A full text search engine like `Whoosh`_ or `Xapian`_. Whoosh is the easiest
    to setup. First, install Whoosh::

      $ pip install whoosh

    The configure Django to use it::

      # To be added to Django's settings.py

      HAYSTACK_CONNECTIONS = {
            'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(BASE_DIR, "fulltext_index"),
            },
      }

    For setup instruction on other search backends, look at the documentation
    for `django-haystack`_.


Setting up Mailman Core
-----------------------

Mailman Core is responsible for sending and receiving emails. It exposes a REST
API that different clients can use to interact with over an HTTP protocol. The
API itself is an administrative API and it is recommended that you don't expose
it to outside of your host or trusted network. To install Core run::

  $ pip install mailman

This _should_ install the latest release of Mailman Core, which is 3.1.0 as of
writing of this document. After this, create a configuration file at
``/etc/mailman.cfg`` for Mailman Core. The absolutely basic required
configuration is listed below::

  # /etc/mailman.cfg


  [mailman]
  # Which paths.* filesystem layout to use.
  layout: fhs

  [mta]
  incoming: mailman.mta.postfix.LMTP
  outgoing: mailman.mta.deliver.deliver
  lmtp_host: 127.0.0.1            # IP where Mailman should listen for emails from MTA
  lmtp_port: 8024
  smtp_host: 127.0.0.1            # IP Where MTA is listening for emails
  smtp_port: 25


Note that the above configuration uses Postfix as your default MTA. To use
something else like Exim4 or qmail, look at the `setting up your MTA`_ docs.

Also, the above configuration uses Sqlite3 database. To use a different
database, please see setting up your `database for Mailman Core`_.

You can change the default location of configuration file by setting up the
environment variable ``MAILMAN_CONFIG_FILE``. You can explore several different
nways to `configure Mailman Core`_.

After this, running ``mailman info`` will give you an output which looks
something like below::

  $ mailman info
  GNU Mailman 3.1.0 (Between The Wheels)
  Python 3.5.3 (default, Jan 19 2017, 14:11:04)
  [GCC 6.3.0 20170118]
  config file: /etc/mailman.cfg
  db url: sqlite:////var/lib/mailman/data/mailman.db
  devmode: DISABLED
  REST root url: http://localhost:8001/3.1/
  REST credentials: restadmin:restpass


Setting up Web UI
-----------------

Postorius and Hyperkitty are Mailman's official Web UI and Archiver. Both of
them are Django based apps and can be integrated into an existing Django
installation/website.

Installing Postorius & Hyperkitty
=================================

Postorius and Hyperkitty are Django "apps" and can be directly installed using
the commands below::

  $ pip install postorius
  $ pip install hyperkitty

What is Django?
===============

Django_ is a Python web framework that allows you to run several different web
"apps" under a single "project". People often write their web applications as
reusable Django "apps" which can then be plugged into any running Django
"project". Django "project" is also sometimes called as a Django "installation".

A typical Django project has a structure of something like this::

  toplevel_project:
  \
   |---- urls.py
   |---- settings.py
   |---- manage.py
   |---- wsgi.py


It is important to understand the above structure even when *using* Django,
because the configuration of a Django project requires you to edit these files.

Here is a brief overview of these files:

  * ``urls.py``: This is how Django routes request. If you want to install a new
    Django "app", you have to add it's ``base_url`` to this file.

  * ``settings.py`` : This is Django's configuration file. It is the home for
    all the different configuration options that are required.

  * ``manage.py`` : This is a generally a helper script used to perform
    administrative tasks from command line on a Django project. You should never
    edit this file after creating a project.

  * ``wsgi.py`` : This is the WSGI_ or Web Server Gateway Interface application
    for the Django project. You will need this file later to interface Django
    with an actual webserver like Nginx or Apache. Usually, you don't need to
    change anything in this file.

If you have absolutely no idea about Django, just clone/download this
`mailman-suite repo`_ . It should have a documented ``settings.py`` and a
pre-configured ``urls.py`` which you can use to run new Django installation. If
you find any setting that is not documented, please look at `Django's settings
reference`_ for all available settings.

Exact commands would look something like this::

  # Download and install the latest release of django.
  $ pip install django

  # Clone the repo locally.
  $ git clone https://gitlab.com/mailman/mailman-suite/
  $ cd mailman-suite/mailman-suite_project/

  # Create the tables in the database and load fixtures.
  $ python manage.py migrate

  # Copy all the static files to one single location.
  $ python manage.py collectstatic

  # Run the Django's "development" server at localhost:8000
  $ python manage.py runserver


Configure Postorius & Hyperkitty
================================

Here are the parameters that will affect Postorius and Hyperkitty will
function. These parameters are configured in your Django's ``settings.py``.

  * ``MAILMAN_REST_API_URL`` : Complete URL to the Core's REST API
    Server. Usually, Mailman Core listens on port 8001 for REST
    API. e.g. ``http://localhost:8001/``

  * ``MAILMAN_REST_API_USER`` : Username for the Core's REST API, default value
    in core is 'restadmin' if not set.

  * ``MAILMAN_REST_API_PASS`` : Password for Mailman REST API User, default
    value in core is 'restpass' if not set.

  * ``MAILMAN_ARCHIVER_KEY`` : The Key used to authenticate the emails for
    archiving in Hyperkitty. Its value should be exactly same as set in Core.

    Also note that the value in ``settings.py`` will be within single quotes,
    but in ``mailman.cfg`` it will be without any quotes.

  * ``FILTER_VHOST`` : Filter the list of available lists in Hyperkitty
    depending on the domain it is being currently served from. Mailman 3
    supports multiple domains in a single installation.

  * Add ``compressor.finders.CompressorFinder`` to your ``STATICFILES_FINDERS``.

  * ``LOGIN_URL`` = 'account_login'

  * ``LOGIN_REDIRECT_URL`` = 'list_index'

  * ``LOGOUT_URL`` = 'account_logout'

  * This setting is for django-compressor which is used here to compile and
    compress static files::

      COMPRESS_PRECOMPILERS = (
        ('text/x-scss', 'sassc -t compressed {infile} {outfile}'),
        ('text/x-sass', 'sassc -t compressed {infile} {outfile}'),
      )


  * Hyperkitty uses `django-haystack`_ for its full text search::

      HAYSTACK_CONNECTIONS = {
            'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(BASE_DIR, "fulltext_index"),
            },
      }


  * Hyperkitty uses `django_q`_ as a task queue. It supports various different
    backends like Redis, Disque, IronMQ, SQS etc. Please check the documentation
    to better understand how to configure it. The most basic setup where it uses
    Django orm as the queue can be configured using the settings below::

      Q_CLUSTER = {
         'timeout': 300,
         'save_limit': 100,
         'orm': 'default',
      }

If your Django site is configured to send emails, there is no additional setup
required for Postorius and Hyperkitty. Here are some settings that determine how
your emails from them look like:

  * ``DEFAULT_FROM_EMAIL`` : This is the default address that used used as the
    ``FROM`` header in all the emails from your django installation.

  * ``SERVER_EMAIL`` : This is the address from which the errors emails will be
    sent to you.


Note that both of these are general Django related settings and will affect
other parts of your Django installation too!

These settings below are required by Django-Allauth, which is used for user
account management.

  * ``ACCOUNT_AUTHENTICATION_METHOD`` = "username_email"
  * ``ACCOUNT_EMAIL_REQUIRED`` = True
  * ``ACCOUNT_EMAIL_VERIFICATION`` = "mandatory"
  * ``ACCOUNT_DEFAULT_HTTP_PROTOCOL`` = "http"
  * ``ACCOUNT_UNIQUE_EMAIL``  = True


Setting up a WSGI server
========================

These instructions are to setup your Django website behind a webserver. We are
using a uwsgi as the wsgi server to communicate between the webserver and
Django. First setup uwsgi using the following configuration file::

  [uwsgi]
  # Port on which uwsgi will be listening.
  http = :8000

  # Move to the directory wher the django files are.
  chdir = /path-to-django-project-directory/

  # Use the wsgi file provided with the django project.
  wsgi-file = wsgi.py

  # Setup default number of processes and threads per process.
  master = true
  process = 2
  threads = 2

  # Drop privielges and don't run as root.
  uid = 1000
  gid = 1000

  # Setup the django_q related worker processes.
  attach-daemon = ./manage.py qcluster

  # Setup the request log.
  req-logger = file:/path-to-logs/logs/uwsgi.log

  # Log cron seperately.
  logger = cron file:/path-to-logs/logs/uwsgi-cron.log
  log-route = cron uwsgi-cron

  # Log qcluster commands seperately.
  logger = qcluster file:/path-to-logs/logs/uwsgi-qcluster.log
  log-route = qcluster uwsgi-daemons

  # Last log and it logs the rest of the stuff.
  logger = file:/path-to-logs/logs/uwsgi-error.log


Note that in the above configuration, there is a command called ``python
manage.py qcluster`` which run the ``django-q`` processes. You can remove this
from here if you want to manage this yourself via some other init process.

Have a look at `uwsgi`_ documentation to learn more about different
configuration options. There are 1000s of them.


Nginx Configuration
===================

You can reverse proxy the requests to uwsgi server using Nginx. Uwsgi has a
special protocol called uwsgi protocol that is available in Nginx as a
plugin. Add the following configuration to your sites-availbale in Nginx::

  server {

        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;

        server_name MY_SERVER_NAME;
        location /static/ {
             alias /path-to-djang-staticdir/static/;
        }
        ssl_certificate /path-to-ssl-certs/cert.pem;
        ssl_certificate_key /path-to-ssl-certs/privkey.pem;

        location / {
                include uwsgi_params;
                uwsgi_pass localhost:8000;

        }

  }

Fill in the appropriate paths above in the configuration before using it.

.. _setting up your MTA: https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html
.. _Postfix: http://www.postfix.org/
.. _Exim4: http://www.exim.org/
.. _sendmail: https://www.proofpoint.com/us/sendmail-open-source
.. _qmail: http://cr.yp.to/qmail.html
   _sendmail setup: https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html#sendmail
.. _qmail setup : https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html#qmail-configuration
.. _django_q : https://django-q.readthedocs.io/en/latest/
.. _uwsgi : https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html
.. _detailed documentation: https://asynchronous.in/docker-mailman/
.. _Github repo for docker-mailman: https://github.com/maxking/docker-mailman
.. _setup email backend for django: https://docs.djangoproject.com/en/1.11/topics/email/#smtp-backend
.. _sass compiler : http://sass-lang.com/
.. _installation guide for sass : http://sass-lang.com/install
.. _Whoosh : https://pythonhosted.org/Whoosh/
.. _Xapian : https://xapian.org/
.. _django-haystack : https://django-haystack.readthedocs.io/en/master/backend_support.html
.. _configure Mailman Core: https://mailman.readthedocs.io/en/latest/src/mailman/config/docs/config.html
.. _database of Mailman Core : https://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html
.. _Django : https://www.djangoproject.com/
.. _WSGI : https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface
.. _mailman-suite repo : https://gitlab.com/mailman/mailman-suite/tree/master
.. _Django's settings reference: https://docs.djangoproject.com/en/1.11/ref/settings/
.. _database for mailman core: https://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html
